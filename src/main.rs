mod problem1;

fn main() {
    println!("Solution to problem 1:");
    println!("{:?}", problem1::solution());
}
